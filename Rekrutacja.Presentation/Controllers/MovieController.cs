﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using Rekrutacja.Application.Dtos;
using System.Net;
using Rekrutacja.Application.Queries.Movies.GetMovies;
using Rekrutacja.Application.Queries.Movies.GetMoviesById;
using Rekrutacja.Application.Queries.Movies.GetMovieByTitle;
using Rekrutacja.Application.Commands.Movies.AddMovie;
using Rekrutacja.Application.Commands.Movies.UpdateMovie;
using Rekrutacja.Application.Commands.Movies.DeleteMovie;
using Rekrutacja.Application.Commands.Movies.SynchronizeMovies;
using System.Net.Http.Json;
using System;

namespace Rekrutacja.Presentation.Controllers
{
    [Route("api/movies")]
    [ApiController]
    public class MovieController(IMediator mediator, IHttpClientFactory httpClientFactory) : Controller
    {
        private readonly IMediator _mediator = mediator;
        private readonly IHttpClientFactory _httpClientFactory = httpClientFactory;

        [HttpGet]
        [SwaggerOperation("Get movies")]
        [ProducesResponseType(typeof(IEnumerable<MovieDto>),(int)HttpStatusCode.OK)]
        public async Task<ActionResult> Get()
        {
            var result = await _mediator.Send(new GetMoviesQuery());

            return Ok(result);
        }

        [HttpGet("{id}")]
        [SwaggerOperation("Get movie by Id")]
        [ProducesResponseType(typeof(IEnumerable<MovieDto>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult> GetById([FromRoute]int id)
        {
            var result = await _mediator.Send(new GetMoviesByIdQuery(id));

            return result != null ? Ok(result) : NotFound();
        }

        [HttpGet("[action]/{title}")]
        [SwaggerOperation("Get movie by title")]
        [ProducesResponseType(typeof(IEnumerable<MovieDto>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult> getByTitle([FromRoute] string title)
        {
            var result = await _mediator.Send(new GetMovieByTitleQuery(title));

            return result != null ? Ok(result) : NotFound();
        }

        [HttpPost]
        [SwaggerOperation("Add movie")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<ActionResult> Post([FromBody] AddMovieCommand command)
        {
            var result = await _mediator.Send(command);

            return CreatedAtAction(nameof(GetById), new { id = result.Id }, result);
        }

        [HttpPut]
        [SwaggerOperation("Update movie")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<ActionResult> Put([FromBody] UpdateMovieCommand command)
        {
            await _mediator.Send(command);

            return NoContent();
        }

        [HttpDelete("{id}")]
        [SwaggerOperation("Delete movie")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<ActionResult> Delete([FromRoute]int id)
        {
            await _mediator.Send(new DeleteMovieCommand(id));

            return NoContent();
        }

        [HttpGet("[action]")]
        [SwaggerOperation("Synchronize movies")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> SynchronizeMovies()
        {
            var httpClient = _httpClientFactory.CreateClient();

            var apiUrl = "https://filmy.programdemo.pl/MyMovies";

            HttpResponseMessage response = await httpClient.GetAsync(apiUrl);

            if (response.IsSuccessStatusCode)
            {
                string responseBody = await response.Content.ReadAsStringAsync();
                var movies = System.Text.Json.JsonSerializer.Deserialize<List<MovieDto>>(responseBody);
                await _mediator.Send(new SynchronizeMoviesCommand(movies));

                return NoContent();
            }
            else
            {
                return StatusCode((int)response.StatusCode, "Błąd podczas pobierania danych z API.");
            }
        }
    }
}

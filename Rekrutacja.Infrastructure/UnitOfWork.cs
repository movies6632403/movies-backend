﻿using Microsoft.EntityFrameworkCore;
using Rekrutacja.Domain.Abstractions;
using Rekrutacja.Domain.Entities;
using Rekrutacja.Infrastructure.Context;

namespace Rekrutacja.Infrastructure
{
    internal class UnitOfWork : IUnitOfWork
    {
        private readonly RekrutacjaDbContext _dbContext;
        public UnitOfWork(RekrutacjaDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task SaveChangesAsync(CancellationToken cancellation = default)
        {
            UpdateAuditableEntities();
            await _dbContext.SaveChangesAsync(cancellation);
        }

        private void UpdateAuditableEntities()
        {
            var entries = _dbContext.ChangeTracker.Entries<Entity>();

            foreach (var entry in entries)
            {
                if (entry.State == EntityState.Added)
                {
                    entry.Entity.CreatedAt = DateTime.UtcNow;
                }
                
                if(entry.State == EntityState.Modified)
                {
                    entry.Entity.UpdatedAt = DateTime.UtcNow;
                }

                if (entry.State == EntityState.Deleted)
                {
                    entry.Entity.DeletedAt = DateTime.UtcNow;
                }
            }
        }
    }
}

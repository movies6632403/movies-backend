﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Rekrutacja.Domain.Abstractions;
using Rekrutacja.Infrastructure.Context;
using Rekrutacja.Infrastructure.Repositories;

namespace Rekrutacja.Infrastructure
{
    public static class Extensions
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IMovieRepository, MovieRepository>();
            services.AddDbContext<RekrutacjaDbContext>(ctx => ctx.UseSqlServer(configuration.GetConnectionString("RekrutacjaCS")));

            return services;
        }
    }
}

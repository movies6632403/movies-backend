﻿using Microsoft.EntityFrameworkCore;
using Rekrutacja.Domain.Entities;
using Rekrutacja.Infrastructure.Config;

namespace Rekrutacja.Infrastructure.Context
{
    public class RekrutacjaDbContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public RekrutacjaDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("rekrutacja");
            modelBuilder.ApplyConfiguration(new MovieConfiguration());
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Rekrutacja.Domain.Abstractions;
using Rekrutacja.Domain.Entities;
using Rekrutacja.Infrastructure.Context;
namespace Rekrutacja.Infrastructure.Repositories
{
    internal class MovieRepository : IMovieRepository
    {
        private readonly RekrutacjaDbContext _dbContext;
        public MovieRepository(RekrutacjaDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<IEnumerable<Movie>> GetAllAsync(CancellationToken cancellation = default)
        {
            return await _dbContext.Movies.ToListAsync(cancellation);
        }

        public async Task<Movie> GetByIdAsync(int id, CancellationToken cancellation = default)
        {
            return await _dbContext.Movies.SingleOrDefaultAsync(x => x.Id == id, cancellation);
        }

        public async Task<Movie> GetByTitleAsync(string title, CancellationToken cancellation = default)
        {
            return await _dbContext.Movies.SingleOrDefaultAsync(x => x.Title == title, cancellation);
        }

        public void Add(Movie movie)
        {
            _dbContext.Movies.Add(movie);
        }

        public void Update(Movie movie)
        {
            _dbContext.Movies.Update(movie);
        }

        public void Delete(Movie movie)
        {
            _dbContext.Movies.Remove(movie);
        }
    }
}

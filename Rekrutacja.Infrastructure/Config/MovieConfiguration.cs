﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Rekrutacja.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Rekrutacja.Infrastructure.Config
{
    public class MovieConfiguration : BaseEntityConfiguration<Movie>
    {
        public override void Configure(EntityTypeBuilder<Movie> builder)
        {
            builder.ToTable("Movies");

            builder.Property(x => x.Title).IsRequired().HasMaxLength(200);
            builder.Property(x => x.Director).IsRequired();
            builder.Property(x => x.Year).IsRequired();
            builder.Property(x => x.Rate).IsRequired();

            base.Configure(builder);
        }
    }
}

﻿using Rekrutacja.Domain.Entities;

namespace Rekrutacja.Domain.Abstractions
{
    public interface IMovieRepository
    {
        Task<IEnumerable<Movie>> GetAllAsync(CancellationToken cancellation = default);
        Task<Movie> GetByIdAsync(int id, CancellationToken cancellation = default);
        Task<Movie> GetByTitleAsync(string title, CancellationToken cancellation = default);

        void Add (Movie movie);
        void Update (Movie movie);
        void Delete (Movie movie);
    }
}

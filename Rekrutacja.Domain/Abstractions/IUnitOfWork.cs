﻿namespace Rekrutacja.Domain.Abstractions
{
    public interface IUnitOfWork
    {
        Task SaveChangesAsync(CancellationToken cancellation = default);
    }
}

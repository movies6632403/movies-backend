﻿using System.ComponentModel.DataAnnotations;

namespace Rekrutacja.Domain.Entities;

public class Movie : Entity
{
    public string Title { get; set; }
    public string Director { get; set; }
    [Range(1900, 2200, ErrorMessage = "Year must be between 1900 and 2200.")]
    public int Year { get; set; }
    public float Rate { get; set; } 
}

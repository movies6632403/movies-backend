﻿using Microsoft.Extensions.DependencyInjection;
using MediatR;
using System.Reflection;
using FluentValidation;

using Rekrutacja.Application.Commands.Movies.AddMovie;
using Rekrutacja.Application.Commands.Movies.UpdateMovie;


namespace Rekrutacja.Application
{
    public static class Extensions
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            var executingAssembly = Assembly.GetExecutingAssembly(); 
            services.AddMediatR(cfg =>cfg.RegisterServicesFromAssemblies(executingAssembly));
            services.AddAutoMapper(executingAssembly);

            services.AddScoped<IValidator<AddMovieCommand>, AddMovieCommandValidation>();
            services.AddScoped<IValidator<UpdateMovieCommand>, UpdateMovieCommandValidation>();

            return services;
        }
    }
}

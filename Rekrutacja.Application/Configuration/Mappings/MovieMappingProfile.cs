﻿using AutoMapper;
using Rekrutacja.Application.Dtos;
using Rekrutacja.Domain.Entities;

namespace Rekrutacja.Application.Configuration.Mappings
{
    public class MovieMappingProfile : Profile
    {
        public MovieMappingProfile()
        {
            CreateMap<Movie, MovieDto>();
        }
    }
}

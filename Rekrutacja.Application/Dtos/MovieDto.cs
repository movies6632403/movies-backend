﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Rekrutacja.Application.Dtos
{
    public class MovieDto
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }
        [JsonPropertyName("title")]
        public string Title { get; set; }
        [JsonPropertyName("director")]
        public string Director { get; set; }
        [Range(1900, 2200, ErrorMessage = "Year must be between 1900 and 2200.")]
        [JsonPropertyName("year")]
        public int Year { get; set; }
        [JsonPropertyName("rate")]
        public float Rate { get; set; }
    }
}

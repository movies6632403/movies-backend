﻿using MediatR;
using Rekrutacja.Application.Dtos;

namespace Rekrutacja.Application.Queries.Movies.GetMovies
{
    public record GetMoviesQuery() : IRequest<IEnumerable<MovieDto>>
    {

    }
}

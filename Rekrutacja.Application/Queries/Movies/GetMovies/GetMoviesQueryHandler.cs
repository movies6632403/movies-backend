﻿using AutoMapper;
using MediatR;
using Rekrutacja.Application.Dtos;
using Rekrutacja.Domain.Abstractions;
using Rekrutacja.Domain.Entities;

namespace Rekrutacja.Application.Queries.Movies.GetMovies
{
    public class GetMoviesQueryHandler(IMovieRepository movieRepository, IMapper mapper) : IRequestHandler<GetMoviesQuery, IEnumerable<MovieDto>>
    {
        private readonly IMovieRepository _movieRepository = movieRepository;
        private readonly IMapper _mapper = mapper;

        public async Task<IEnumerable<MovieDto>> Handle(GetMoviesQuery request, CancellationToken cancellationToken)
        {
            var movies = await _movieRepository.GetAllAsync(cancellationToken);

            var moviesDto = _mapper.Map<IEnumerable<MovieDto>>(movies);

            return moviesDto;
        }
    }
}

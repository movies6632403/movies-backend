﻿using MediatR;
using Rekrutacja.Application.Dtos;

namespace Rekrutacja.Application.Queries.Movies.GetMovieByTitle
{
    public record GetMovieByTitleQuery(string Title) : IRequest<MovieDto>
    {
    }
}

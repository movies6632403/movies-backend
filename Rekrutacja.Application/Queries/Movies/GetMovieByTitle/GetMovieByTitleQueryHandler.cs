﻿using AutoMapper;
using MediatR;
using Rekrutacja.Application.Dtos;
using Rekrutacja.Application.Queries.Movies.GetMovieByTitle;
using Rekrutacja.Domain.Abstractions;

namespace Rekrutacja.Application.Queries.Movies.GetMoviesByTitle
{
    internal class GetMovieByTitleQueryHandler : IRequestHandler<GetMovieByTitleQuery, MovieDto>
    {
        private readonly IMovieRepository _movieRepository;
        private readonly IMapper _mapper;
        public GetMovieByTitleQueryHandler(IMovieRepository movieRepository, IMapper mapper)
        {
            _movieRepository = movieRepository;
            _mapper = mapper;
        }

        public async Task<MovieDto> Handle(GetMovieByTitleQuery request, CancellationToken cancellationToken)
        {
            var movie = await _movieRepository.GetByTitleAsync(request.Title, cancellationToken);

            if (movie == null)
            {
                throw new Exception($"Movie with Title: {request.Title} not found");
            }

            var movieDto = _mapper.Map<MovieDto>(movie);

            return movieDto;
        }
    }
}

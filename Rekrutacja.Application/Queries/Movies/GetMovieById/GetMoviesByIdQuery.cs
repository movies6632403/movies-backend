﻿using MediatR;
using Rekrutacja.Application.Dtos;

namespace Rekrutacja.Application.Queries.Movies.GetMoviesById
{
    public record GetMoviesByIdQuery(int Id) : IRequest<MovieDto>
    {
    }
}

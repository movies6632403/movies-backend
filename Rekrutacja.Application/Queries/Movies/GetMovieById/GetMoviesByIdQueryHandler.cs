﻿using AutoMapper;
using MediatR;
using Rekrutacja.Application.Dtos;
using Rekrutacja.Domain.Abstractions;

namespace Rekrutacja.Application.Queries.Movies.GetMoviesById
{
    internal class GetMoviesByIdQueryHandler : IRequestHandler<GetMoviesByIdQuery, MovieDto>
    {
        private readonly IMovieRepository _movieRepository;
        private readonly IMapper _mapper;
        public GetMoviesByIdQueryHandler(IMovieRepository movieRepository, IMapper mapper)
        {
            _movieRepository = movieRepository;
            _mapper = mapper;
        }

        public async Task<MovieDto> Handle(GetMoviesByIdQuery request, CancellationToken cancellationToken)
        {
            var movie = await _movieRepository.GetByIdAsync(request.Id, cancellationToken);
            
            if (movie == null)
            {
                throw new Exception($"Movie with Id: {request.Id} not found");
            }

            var movieDto = _mapper.Map<MovieDto>(movie);

            return movieDto;
        }
    }
}

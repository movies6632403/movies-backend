﻿using MediatR;
using Rekrutacja.Application.Dtos;
using Rekrutacja.Domain.Abstractions;

namespace Rekrutacja.Application.Commands.Movies.DeleteMovie
{
    internal class DeleteMovieCommandHandler(IMovieRepository movieRepository, IUnitOfWork unitOfWork) : IRequestHandler<DeleteMovieCommand>
    {
        private readonly IMovieRepository _movieRepository = movieRepository;
        private readonly IUnitOfWork _unitOfWork = unitOfWork;

        public async Task Handle(DeleteMovieCommand request, CancellationToken cancellationToken)
        {
            var movie = await _movieRepository.GetByIdAsync(request.id, cancellationToken) ?? throw new Exception("Movie with Id: {request.Id} not found");

            _movieRepository.Delete(movie);
            await _unitOfWork.SaveChangesAsync(cancellationToken);
        }
    }
}

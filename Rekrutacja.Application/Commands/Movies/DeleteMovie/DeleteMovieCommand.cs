﻿using MediatR;

namespace Rekrutacja.Application.Commands.Movies.DeleteMovie
{
    public record DeleteMovieCommand(int id) : IRequest
    {
    }
}

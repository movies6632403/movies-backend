﻿using MediatR;
using Rekrutacja.Application.Dtos;

namespace Rekrutacja.Application.Commands.Movies.AddMovie
{
    public record AddMovieCommand : IRequest<MovieDto>
    {
        public string Title { get; set; }
        public string Director { get; set; }
        public int Year { get; set; }
        public float Rate { get; set; }
    }
}

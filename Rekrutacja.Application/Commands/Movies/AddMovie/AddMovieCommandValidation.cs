﻿using FluentValidation;
namespace Rekrutacja.Application.Commands.Movies.AddMovie
{
    public class AddMovieCommandValidation : AbstractValidator<AddMovieCommand>
    {
        public AddMovieCommandValidation()
        {
            RuleFor(x => x.Title)
                .NotEmpty().WithMessage("Title is required")
                .MaximumLength(200).WithMessage("Title must be less than 200 characters");

            RuleFor(x => x.Director)
                .NotEmpty()
                .WithMessage("Director is required");

            RuleFor(x => x.Year)
                .NotEmpty().WithMessage("Year is required")
                .GreaterThanOrEqualTo(1900).WithMessage("Year value must be greater or equal to 1900")
                .LessThanOrEqualTo(2200).WithMessage("Year value must be less or equal to 2200");

            RuleFor(x => x.Rate)
                .NotEmpty()
                .WithMessage("Rate is required");
        }
    }
}

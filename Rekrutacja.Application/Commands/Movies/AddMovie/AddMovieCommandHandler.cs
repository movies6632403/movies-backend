﻿using AutoMapper;
using FluentValidation;
using FluentValidation.Results;
using MediatR;
using Rekrutacja.Application.Configuration.Mappings;
using Rekrutacja.Application.Dtos;
using Rekrutacja.Domain.Abstractions;
using Rekrutacja.Domain.Entities;


namespace Rekrutacja.Application.Commands.Movies.AddMovie
{
    internal class AddMovieCommandHandler(IMovieRepository movieRepository, IUnitOfWork unitOfWork, IMapper mapper, IValidator<AddMovieCommand> validator) : IRequestHandler<AddMovieCommand, MovieDto>
    {
        private readonly IMovieRepository _movieRepository = movieRepository;
        private readonly IUnitOfWork _unitOfWork = unitOfWork;
        private readonly IMapper _mapper = mapper;
        private readonly IValidator<AddMovieCommand> _validator = validator;

        public async Task<MovieDto> Handle(AddMovieCommand request, CancellationToken cancellationToken)
        {
            ValidationResult result = _validator.Validate(request);

            if(!result.IsValid)
            {
                var errorList = result.Errors.Select(x => x.ErrorMessage);
                throw new ValidationException($"Invalid command, reasons: {string.Join(',', errorList.ToArray())}");
            }

            var isAlreadyExist = await _movieRepository.GetByTitleAsync(request.Title, cancellationToken);
            
            if (isAlreadyExist != null)
            {
                throw new Exception($"Movie with title: {request.Title} already exist");
            }
            
            var newMovie = new Movie() {
                Title = request.Title,
                Director = request.Director,
                Year = request.Year,
                Rate = request.Rate
            };

            _movieRepository.Add(newMovie);
            await _unitOfWork.SaveChangesAsync(cancellationToken);

            var movieDto = _mapper.Map<MovieDto>(newMovie);

            return movieDto;
        }
    }
}

﻿using FluentValidation;
using FluentValidation.Results;
using MediatR;
using Rekrutacja.Domain.Abstractions;

namespace Rekrutacja.Application.Commands.Movies.UpdateMovie
{
    internal class UpdateMovieCommandHandler(IMovieRepository movieRepository, IUnitOfWork unitOfWork, IValidator<UpdateMovieCommand> validator) : IRequestHandler<UpdateMovieCommand>
    {
        private readonly IMovieRepository _movieRepository = movieRepository;
        private readonly IUnitOfWork _unitOfWork = unitOfWork;
        private readonly IValidator<UpdateMovieCommand> _validator = validator;

        public async Task Handle(UpdateMovieCommand request, CancellationToken cancellationToken)
        {

            ValidationResult result = _validator.Validate(request);

            if (!result.IsValid)
            {
                var errorList = result.Errors.Select(x => x.ErrorMessage);
                throw new ValidationException($"Invalid command, reasons: {string.Join(',', errorList.ToArray())}");
            }
            var movie = await _movieRepository.GetByIdAsync(request.Id, cancellationToken) ?? throw new Exception("Movie with Id: {request.Id} not found");

            movie.Title = request.Title;
            movie.Director = request.Director;
            movie.Year = request.Year;
            movie.Rate = request.Rate;

            _movieRepository.Update(movie);
            await _unitOfWork.SaveChangesAsync(cancellationToken);
        }
    }
}

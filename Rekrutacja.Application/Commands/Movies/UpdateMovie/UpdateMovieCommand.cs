﻿using MediatR;

namespace Rekrutacja.Application.Commands.Movies.UpdateMovie
{
    public record UpdateMovieCommand : IRequest
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Director { get; set; }
        public int Year { get; set; }
        public float Rate { get; set; }
    }
}
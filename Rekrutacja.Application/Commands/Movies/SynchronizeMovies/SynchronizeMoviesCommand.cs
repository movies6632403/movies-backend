﻿using MediatR;
using Rekrutacja.Application.Dtos;

namespace Rekrutacja.Application.Commands.Movies.SynchronizeMovies
{
    public record SynchronizeMoviesCommand(IEnumerable<MovieDto> Movies) : IRequest
    {
    }
}

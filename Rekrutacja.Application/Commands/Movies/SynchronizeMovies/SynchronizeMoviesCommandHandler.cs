﻿using MediatR;
using Rekrutacja.Application.Commands.Movies.SynchronizeMovies;
using Rekrutacja.Application.Dtos;
using Rekrutacja.Domain.Abstractions;
using Rekrutacja.Domain.Entities;

namespace Rekrutacja.Application.Commands.Movies.DeleteMovie
{
    internal class SynchronizeMoviesCommandHandler(IMovieRepository movieRepository, IUnitOfWork unitOfWork) : IRequestHandler<SynchronizeMoviesCommand>
    {
        private readonly IMovieRepository _movieRepository = movieRepository;
        private readonly IUnitOfWork _unitOfWork = unitOfWork;

        public async Task Handle(SynchronizeMoviesCommand request, CancellationToken cancellationToken)
        {
            try
            {
                foreach (var movieDto in request.Movies)
                {
                    var existingMovie = await _movieRepository.GetByTitleAsync(movieDto.Title, cancellationToken);

                    if (existingMovie == null)
                    {
                        var newMovie = new Movie
                        {
                            Title = movieDto.Title,
                            Director = movieDto.Director,
                            Year = movieDto.Year,
                            Rate = movieDto.Rate
                        };

                        _movieRepository.Add(newMovie);
                    }
                    else
                    {
                        existingMovie.Director = movieDto.Director;
                        existingMovie.Year = movieDto.Year;
                        existingMovie.Rate = movieDto.Rate;
                    }
                }
                await _unitOfWork.SaveChangesAsync(cancellationToken);

            }
            catch (Exception ex)
            {
                throw new Exception("Syncing videos Error", ex);
            }
        }
    }
}
